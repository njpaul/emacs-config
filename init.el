;; Load packages because some init code depends on the packages.
;; Disable automatic package loading after init because we just loaded the
;; packages.
(package-initialize)
(setq package-enable-at-startup nil)
    
(let ((config-path "~/.emacs.d/config/")
      (config-files '("repos.el" "packages.el" "generic-ui.el"
                      "linum-relative.el" "fill-column-indicator.el"
                      "evil-leader.el" "evil.el"
                      "god-mode.el" "evil-god-state.el"
                      "keybindings.el")))
  (progn
    ;; Load all the configuration files
    (mapc (lambda (file) (load-file (concat config-path file)))
          config-files)
    
    ;; Any changes through Customize are stored in a different file
    (setq custom-file (concat config-path "custom.el"))
    (load-file custom-file)))
