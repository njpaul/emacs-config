;; god-mode
;; Set escape to get us in and out of god mode. It's shadowed when in evil mode,
;; so that ESC works as expected in that mode
(global-set-key (kbd "<escape>") 'god-local-mode)

;; evil-mode
(global-set-key (kbd "M-<escape>") 'evil-mode)

;; evil-god-state
;; TODO: Can the below be a function that knows to enter god state on ESC if
;; the command buffer is empty? Otherwise clear the buffer.
;;(evil-define-key 'normal global-map "," 'evil-execute-in-god-state)
(evil-define-key 'god global-map [escape] 'evil-god-state-bail)

;; Other
(global-set-key (kbd "C-c C-n") 'linum-mode)
