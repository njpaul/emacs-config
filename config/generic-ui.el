;; Select a theme
(load-theme 'zenburn t)

;; Default indentation
(setq-default indent-tabs-mode nil ; Use spaces only
              tab-stop-list '(4)   ; 4 spaces
              tab-width 4)         ; Display true tabs with a width of 4

;; Disable line wrapping 
(setq-default truncate-lines t)

;; Set the fill column to 80 characters
(setq-default fill-column 80)
