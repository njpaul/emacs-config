(require 'fill-column-indicator)
(setq-default fci-rule-color "#800000")

;; Define a new global minor mode for the fci that will allow all buffers
;; to have fci-mode enabled by default. The alternative is to use a hook
;; for each major mode that should have it enabled. This is more thorough
;; than using hooks. To disable it for specific major modes, hooks can be
;; used, but in general we will want it enabled.
(define-globalized-minor-mode
  global-fci-mode
  fci-mode
  (lambda () (fci-mode 1)))

;; Turn it on by default
(global-fci-mode 1)
