;; TODO: Make this more robust against download failures
;; This is inspired by http://stackoverflow.com/questions/10092322/how-to-automatically-install-emacs-packages-by-specifying-a-list-of-package-name
(let ((required-packages '(evil linum-relative fill-column-indicator
                           zenburn-theme god-mode evil-god-state
                           rust-mode evil-leader)))
  ;; Get the latest versions of the packages if the directory containing the
  ;; packages don't exist
  (unless (file-exists-p package-user-dir)
    (package-refresh-contents))
  
  ;; Install any missing packages
  (dolist (package required-packages)
    (unless (package-installed-p package)
      (package-install package))))
  
