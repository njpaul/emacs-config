(require 'linum-relative)
(custom-set-variables
 '(linum-relative-current-symbol "") ; Display actual line number for the current line
 '(linum-relative-format "%1s"))     ; Show only the minimum amount of line number characters required
