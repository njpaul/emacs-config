(require 'evil-leader)
(global-evil-leader-mode)

(evil-leader/set-key
 "n" 'linum-mode
 "g" 'evil-execute-in-god-state)
