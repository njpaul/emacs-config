;; Third-party repositories
(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/")
             t)
             
;;(add-to-list 'package-archives
;;             '("melpa-stable" . "http://stable.melpa.org/packages/")
;;             t)

(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/")
             t)
